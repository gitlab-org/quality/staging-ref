# Staging Ref

CI configuration to run QA pipelines against Staging Ref environment.

## Notes

GitLab QA allows [setting](https://gitlab.com/gitlab-org/gitlab-qa/-/blob/master/docs/what_tests_can_be_run.md#supported-gitlab-environment-variables) the Chrome browser user agent with `GITLAB_QA_USER_AGENT`.
This is used to bypass login challenges that don't exist on Staging Ref, so it is unset here.
